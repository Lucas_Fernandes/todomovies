package com.teste.todomovies.network

import com.teste.todomovies.model.GenresResponseModel
import com.teste.todomovies.model.MovieDetailsResponseModel
import com.teste.todomovies.model.SimilarMoviesResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitApi {

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId:String,
                        @Query("api_key") apiKey: String): Single<MovieDetailsResponseModel>

    @GET("movie/{movie_id}/similar")
    fun getSimilarMovies( @Path("movie_id") movieId:String,
                          @Query("api_key") apiKey: String): Single<SimilarMoviesResponseModel>

    @GET("genre/movie/list")
    fun getGenresList(@Query("api_key") apiKey: String): Single<GenresResponseModel>
}