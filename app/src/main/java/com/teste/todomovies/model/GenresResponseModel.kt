package com.teste.todomovies.model

import com.google.gson.annotations.SerializedName

data class GenresResponseModel(
    @SerializedName("genres")
    val genresList: List<MovieGenres>?
)

data class MovieGenres(
    @SerializedName("id")
    val genreId: Int?,

    @SerializedName("name")
    val genreName: String?,
)