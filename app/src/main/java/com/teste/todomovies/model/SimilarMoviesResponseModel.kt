package com.teste.todomovies.model

import com.google.gson.annotations.SerializedName

data class SimilarMoviesResponseModel(
    @SerializedName("results")
    val resultsList: List<SimilarMovies>?
)

data class SimilarMovies(
    @SerializedName("release_date")
    val releaseDate: String?,

    @SerializedName("original_title")
    val originalTitle: String?,

    @SerializedName("genre_ids")
    val genresIds: List<Int>?,

    @SerializedName("backdrop_path")
    val backdropUrl: String?
)