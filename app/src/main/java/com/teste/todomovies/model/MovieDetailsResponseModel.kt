package com.teste.todomovies.model

import com.google.gson.annotations.SerializedName

data class MovieDetailsResponseModel(
    @SerializedName("title")
    val movieTitle: String?,

    @SerializedName("vote_count")
    val likesNumber: String?,

    @SerializedName("popularity")
    val popularity: String?,

    @SerializedName("backdrop_path")
    val posterPath: String?
)