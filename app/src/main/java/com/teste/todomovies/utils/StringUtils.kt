package com.teste.todomovies.utils

fun String.remoteSpecialChars() =
    this.replace("[", "")
        .replace("]", "")