package com.teste.todomovies.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.teste.todomovies.const.Const.API_KEY
import com.teste.todomovies.model.MovieDetailsResponseModel
import com.teste.todomovies.model.MovieGenres
import com.teste.todomovies.model.SimilarMovies
import com.teste.todomovies.network.RetrofitApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(private val apiService: RetrofitApi) : ViewModel() {

    private val disposable = CompositeDisposable()
    private var movieGenresList: MutableList<MovieGenres> = mutableListOf()
    private var similarMoviesResponseList: MutableList<SimilarMovies> = mutableListOf()

    val movieDetails = MutableLiveData<MovieDetailsResponseModel>()
    val similarMovies = MutableLiveData<List<SimilarMovies>>()
    val similarMoviesListString = MutableLiveData<List<List<String?>?>>()


    fun getData() {
        callServices()
    }

    private fun callServices() {
        disposable.add(
            apiService.getGenresList(API_KEY)
                .subscribeOn(Schedulers.io())
                .flatMap { genresListResponse ->
                    genresListResponse.genresList?.toMutableList()
                        ?.let { movieGenresList.addAll(it) }
                    apiService.getSimilarMovies(movieId = "11", apiKey = API_KEY)
                }
                .flatMap { similarMoviesResponse ->
                    similarMoviesResponse.resultsList?.let { similarMoviesResponseList.addAll(it) }
                    apiService.getMovieDetails(movieId = "11", apiKey = API_KEY)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MovieDetailsResponseModel>() {
                    override fun onSuccess(response: MovieDetailsResponseModel) {

                        val similarMoviesListStringResult = similarMoviesResponseList.map { similarMovie ->
                            similarMovie.genresIds?.let {
                                movieGenresList.map { it.genreId to it.genreName }
                                    .toMap()
                                    .filter { f -> it.contains(f.key)}.values
                            }?.toList()
                        }

                        movieDetails.value = response
                        similarMoviesListString.value = similarMoviesListStringResult
                        similarMovies.value = similarMoviesResponseList
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}