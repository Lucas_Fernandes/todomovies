package com.teste.todomovies.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.teste.todomovies.R
import com.teste.todomovies.const.Const.SIMILAR_MOVIES_IMAGE_URL
import com.teste.todomovies.model.SimilarMovies
import com.teste.todomovies.utils.remoteSpecialChars
import kotlinx.android.synthetic.main.item_similar_movies.view.*
import javax.inject.Inject

class ItemSimilarMoviesRVAdapter() :
    RecyclerView.Adapter<ItemSimilarMoviesRVAdapter.ViewHolder>() {

    lateinit var layoutInflater: LayoutInflater
    lateinit var glide: RequestManager

    @Inject
    constructor(inflater: LayoutInflater, requestManager: RequestManager) : this() {
        this.glide = requestManager
        this.layoutInflater = inflater
    }

    private var similarMoviesMainList: MutableList<SimilarMovies> = mutableListOf()
    private var similarMoviesGenres: MutableList<List<List<String?>?>> = mutableListOf()

    fun updateAdapter(similarMoviesList: List<SimilarMovies>,
                      similarMoviesGenreList: List<List<String?>?>) {

        similarMoviesMainList.apply {
            clear()
            addAll(similarMoviesList)
        }

        similarMoviesGenres.apply {
            clear()
            add(similarMoviesGenreList)
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(layoutInflater.inflate(R.layout.item_similar_movies, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(similarMoviesMainList[position], similarMoviesGenres[0][position])

    override fun getItemCount(): Int = similarMoviesMainList.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mainListItem: SimilarMovies, genresList: List<String?>?) {
            val genres = genresList.toString().remoteSpecialChars()

            itemView.apply {
                val imageUrl = "${SIMILAR_MOVIES_IMAGE_URL}/${mainListItem.backdropUrl}"

                val yearWithGenres = "${mainListItem.releaseDate?.take(4)}    $genres"
                glide.load(imageUrl).into(item_similar_movie_poster)

                item_similar_movie_title.text = mainListItem.originalTitle
                item_similar_year_and_genre.text = yearWithGenres
            }
        }
    }
}