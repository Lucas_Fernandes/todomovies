package com.teste.todomovies.view

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.teste.todomovies.R
import com.teste.todomovies.const.Const.POSTER_IMAGE_URL
import com.teste.todomovies.di.factory.ViewModelFactory
import com.teste.todomovies.view.adapter.ItemSimilarMoviesRVAdapter
import com.teste.todomovies.viewModel.MainViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    @Inject
    lateinit var rvAdapter: ItemSimilarMoviesRVAdapter

    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }

        viewModel.getData()

        rv_similar_movies.apply {
            adapter = rvAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        var genresListString: List<List<String?>?> = listOf()

        viewModel.apply {
            movieDetails.observe(this@MainActivity, { movieDetailsResponse ->
                movieDetailsResponse?.let {
                    val imageUrl = "$POSTER_IMAGE_URL${it.posterPath}"
                    val popularityView = "${it.popularity} views"

                    glide.load(imageUrl).into(movie_poster)

                    movie_title.text = it.movieTitle
                    number_of_likes.text = it.likesNumber
                    popularity_number.text = popularityView
                }
            })

            similarMoviesListString.observe(this@MainActivity, { genresString ->
                genresListString = genresString
            })

            similarMovies.observe(this@MainActivity, { similarMovies ->
                rvAdapter.updateAdapter(similarMovies, genresListString)
            })
        }
    }
}