package com.teste.todomovies.di.component

import android.app.Application
import com.teste.todomovies.application.BaseApplication
import com.teste.todomovies.di.module.ActivityBuildersModule
import com.teste.todomovies.di.module.AppModule
import com.teste.todomovies.di.module.RetrofitModule
import com.teste.todomovies.di.module.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        AppModule::class,
        RetrofitModule::class,
        ViewModelFactoryModule::class
    ]
)
interface AppComponent : AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }
}