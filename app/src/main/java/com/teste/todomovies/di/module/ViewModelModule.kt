package com.teste.todomovies.di.module

import androidx.lifecycle.ViewModel
import com.teste.todomovies.di.annotation.ViewModelKey
import com.teste.todomovies.viewModel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindViewModel(mainViewModel: MainViewModel): ViewModel
}
