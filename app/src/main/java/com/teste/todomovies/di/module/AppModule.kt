package com.teste.todomovies.di.module

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.teste.todomovies.R
import com.teste.todomovies.view.adapter.ItemSimilarMoviesRVAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun progressDrawable(application: Application): CircularProgressDrawable =
        CircularProgressDrawable(application).apply {
            setColorSchemeColors(ContextCompat.getColor(application, android.R.color.white))
            strokeWidth = 10f
            centerRadius = 50f
            start()
        }

    @Singleton
    @Provides
    fun provideRequestOptions(circularProgressDrawable: CircularProgressDrawable): RequestOptions =
        RequestOptions.placeholderOf(circularProgressDrawable).error(R.drawable.ic_error_loading)

    @Singleton
    @Provides
    fun provideGlideInstance(application: Application, requestOptions: RequestOptions):
            RequestManager = Glide.with(application).setDefaultRequestOptions(requestOptions)

    @Singleton
    @Provides
    fun provideLayoutInflater(application: Application): LayoutInflater =
        application.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @Singleton
    @Provides
    fun provideRecyclerViewAdapterInstance(layoutInflater: LayoutInflater,
                                           requestManager: RequestManager):
            ItemSimilarMoviesRVAdapter = ItemSimilarMoviesRVAdapter(layoutInflater, requestManager)
}