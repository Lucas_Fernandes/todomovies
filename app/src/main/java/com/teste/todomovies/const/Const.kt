package com.teste.todomovies.const

object Const {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val SIMILAR_MOVIES_IMAGE_URL = "https://image.tmdb.org/t/p/w300"
    const val POSTER_IMAGE_URL = "https://image.tmdb.org/t/p/original"
    const val API_KEY = "8ee5ebcc7de7ac6e7ee3ccee3ead7c7a"
}